#include <stdio.h>

#define LIMIT 100

long double factorial(long double);
long double _factorial(long double,int);

int main(int argc, char **argv) {
    
    int n = 0;
    int m = 0;
    char buffer[LIMIT] = { 0 };
    
    printf("Please input the columns and rows (format:n m):");
    fgets(buffer,LIMIT,stdin);
    sscanf (buffer,"%d %d",&n,&m);
    
    if (n > 0 && m > 0) {        
        printf("Result: %.0Lf\n",factorial(n+m)/(factorial(n)*factorial(m)));                
    }
    else {
        printf("Error - Must be greater than zero\n");
    }
}

long double factorial(long double m) {
    return _factorial(m,(int)m);
}

long double _factorial(long double m,int n) {
    if (n == 1) {
        return m;
    }
    else {        
        n--;
        return _factorial(m*n,n);
    }    
}