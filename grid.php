 <?php
 function factorial($n) {
     if ($n == 1) {
         return $n;
     }
     else {
         return $n * factorial($n-1);
     }
 }

 if (count($argv) == 3) {

     $n = $argv['1'];
     $m = $argv['2'];

     if ($n && is_numeric($n) && $m && is_numeric($m)) {
         echo (factorial($n+$m))/(factorial($n)*factorial($m)) , "\n";
         return;

     }
 }

 echo "Usage: grid N M\n";
