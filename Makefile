CFLAGS=-Wall -g -std=c99 -o
CC=gcc
FILE=grid
	
all: grid.c
	$(CC) $(CFLAGS) $(FILE) $<

clean:
	rm -rf $(FILE)